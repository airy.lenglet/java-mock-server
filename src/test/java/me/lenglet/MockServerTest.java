package me.lenglet;

import org.junit.jupiter.api.Test;
import org.mockserver.file.FileReader;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.logging.MockServerLogger;
import org.mockserver.serialization.ExpectationSerializer;

public class MockServerTest {

    @Test
    void test() {
        final ExpectationSerializer expectationSerializer = new ExpectationSerializer(new MockServerLogger(MockServerTest.class));
        final ClientAndServer clientAndServer = ClientAndServer.startClientAndServer(8080);
        clientAndServer.sendExpectation(expectationSerializer.deserializeArray(FileReader.readFileFromClassPathOrPath(this.getClass().getResource("/mock-server-init.json").getPath())));
    }
}
